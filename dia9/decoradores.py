def nuevo_decorador(a_func):
    def envuelveLaFuncion():
        print("Haciendo algo antes de llamar a a_func()")
        a_func()
        print("Haciendo algo después de llamar a a_func()")
    return envuelveLaFuncion


def funcion_a_decorar():
    print("Soy la función que necesita ser decorada")


funcion_a_decorar()
print("*****************")
funcion_decorada = nuevo_decorador(funcion_a_decorar)

funcion_decorada()
print("*****************")

@nuevo_decorador
def nueva_funcion_decorada():
    print("Otra función que necesita ser decorada")

nueva_funcion_decorada()