from lista_numeros import get_impares, get_pares
import sys

def read_file(filename):
    print(f"Leyendo en el fichero {filename}")
    try:
        with open(filename) as file:
            # for line in file.readlines():
            #     print(line)
            print(file.read()) # Corrección de clase. Se lee directamente el contenido, sin necesidad de bucle.
    except FileNotFoundError as e:
        print(f"Fichero no encontrado: {e}")
    # except:
    #     print(f"Fichero no encontrado: ", sys.exc_info()) # Recogida la excepción de forma genérica
    finally:
        print("Función finalizada")


def write_file(filename):
    print(f"Escribiendo en el fichero {filename}")
    try:
        with open(filename, 'x') as file:
            pares = get_pares(50)
            impares = get_impares(50)
            file.writelines(str(pares)+"\n")
            file.writelines(str(impares))
    except FileExistsError as e:
        raise (f"El fichero {filename} ya existe: {e}")


if __name__ == "__main__":
    if (len(sys.argv) == 2):
        filename = sys.argv[1]
        try:
            write_file(filename)
        except:
            print("El fichero ya existe")
        else:
            read_file(filename)
    else:
        print("Número de parámetros incorrecto")
    # read_file("lorem_ipsum.txt")
    # Leer ficheros con path absoluto
    # read_file('C:\\Users\\User\\Documents\\CursoPython\\ejercicios-modulos\\dia8\\app\\pedidos\\vistas.py')
    # Leer ficheros con path relativo
    #read_file(".\\..\\test.py")
