def get_pares(items=50):
    return [numero for numero in range(1,items*2+1) if numero % 2 == 0]


def get_impares(items=50):
    return [numero for numero in range(items*2+1) if numero % 2 != 0]