from app.pedidos import dao as p_dao, vistas as p_vistas
from app.usuarios import dao as u_dao, vistas as u_vistas


def muestra_datos_archivos():
    print("Archivo principal:")
    print(__file__)
    print(__name__)
    print("Paquete pedidos")
    p_dao.muestra_datos()
    p_vistas.muestra_datos()
    print("Paquete usuarios")
    u_dao.muestra_datos()
    u_vistas.muestra_datos()


if __name__ == "__main__":
    muestra_datos_archivos()
