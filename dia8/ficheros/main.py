from lista_numeros import get_impares, get_pares


def read_file(filename):
    try:
        with open(filename) as file:
            # for line in file.readlines():
            #     print(line)
            print(file.read()) # Corrección de clase. Se lee directamente el contenido, sin necesidad de bucle.
    except FileNotFoundError as e:
        print(f"Fichero no encontrado: {e}")
    # except:
    #     print(f"Fichero no encontrado: ", sys.exc_info()) # Recogida la excepción de forma genérica
    finally:
        print("Función finalizada")


def write_file(filename):
    try:
        with open(filename, 'w') as file:
            pares = get_pares(50)
            impares = get_impares(50)
            print(pares)
            print(impares)
            file.writelines(pares)
            file.writelines(impares)
    except FileExistsError as e:
        print(f"El fichero {filename} ya existe: {e}")


if __name__ == "__main__":
    write_file('pares_impares.txt')
    read_file("lorem_ipsum.txt")
    # Leer ficheros con path absoluto
    read_file('C:\\Users\\User\\Documents\\CursoPython\\ejercicios-modulos\\dia8\\app\\pedidos\\vistas.py')
    # Leer ficheros con path relativo
    read_file(".\\..\\test.py")
