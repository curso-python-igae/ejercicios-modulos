import pathlib
import os
import sys
import test


#muestra todas la rutas en las que buyscará Python
print(sys.path)
for path in sys.path:
    print(path)

print(pathlib.Path(__file__).parent.absolute())
print(os.path.dirname(os.path.abspath(__file__)))
print(__file__)
print(__name__)

print("Modulo importado test")
test.muestra_datos()
